from django.db import models

class TodoItem(models.Model):
    description = models.TextField()
    completed = models.BooleanField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.description
