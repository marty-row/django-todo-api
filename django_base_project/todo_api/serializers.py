from .models import TodoItem
from rest_framework import serializers
from django.http import HttpResponse

class TodoItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TodoItem
        fields = ('id', 'description', 'completed', 'created', 'updated')

    def create(self, validated_data):
        return TodoItem.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.description = validated_data.get('description', instance.description)
        instance.completed = validated_data.get('completed', instance.completed)
        instance.save()
        return instance

    def delete(self, instance):
        instance.delete()
        return HttpResponse(status=204)
