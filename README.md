# Django Todo List API Example
---
This is a bare-bones example of using the Django REST Framework for a basic Todo List API.

## Quick Start
---
Installation Steps

1. It is assumed you have a valid version of Python installed
2. git clone __git@bitbucket.org:marty-row/django-todo-api.git__
3. Using your console/terminal of choice, browse to your workspace folder, type in the following:
    1. cd django_base_project
    2. pip install -r requirements.txt
    3. python manage.py migrate
    4. python manage.py runserver

## Using the API
Open your browser and use the address [http://localhost:8000/todoitems/](http://localhost:8000/todoitems/)

You should see the Django REST Framework page. Here you can create, read, update and delete Todo Items.

## Requirements
---
1. Python 3.5+
2. Django 2.2
3. Django Rest Framework 3.9.x
