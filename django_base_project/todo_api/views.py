from django.shortcuts import render
from rest_framework import viewsets
from .serializers import TodoItemSerializer
from .models import TodoItem

class TodoViewSet(viewsets.ModelViewSet):
    queryset = TodoItem.objects.all()
    serializer_class = TodoItemSerializer
